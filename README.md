# Beam Model Checksum Script #

This folder contains two PowerShell scripts used to verify that the
Monaco beam model is not changing.  The script is currently run nightly
 using the Windows Task scheduler.  For more information
please talk to Janet Hendry or Randy Taylor

The two scripts are as follows:

1. gen\_ref\_data.ps1 crawls all monaco\rtp1\\*\tele\ directories,
calculates their md5 hash and outputs that to expected\_files.txt.  Any
time a legitimate change is made to a beam model, this script needs to
be run to update the MD5 hashes of the files.  This script can take
half an hour or more to run.  For information on configuring it, see
the gen\_ref\_data.ps1 file.

2. check\_bm.ps1 which crawls the same directory and compares the md5
hashes with the reference versions found in expected\_files.txt. If any
changes are found (including new files, deleted files or modified
files) an email is sent to the recipients specified in the script.
Currently this script is automatically run nightly using the Windows
task scheduler.  It can also be run on demand using PowerShell or from
the Windows Task Scheduler.

Output from each run is written to log.txt. The log file contains information
about when the script was run, how many files were checked and the names of
any files that were changed.

The file run_script.bat can be used by the Windows Task Scheduler to schedule
nightly checks of the beam model data. The start time, finish time and any
error output from the scheduled run will be logged in run_log.txt

Configuration (directories and user email addresses) is done via the config
file settings.ps1

Randy Taylor(rataylor@toh.on.ca) - 1 Oct 2012

