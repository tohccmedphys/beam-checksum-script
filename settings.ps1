﻿# set rtp_root to the full path of the location of the Monaco\rtp1\ directory
$rtp_root = "\\your_server_here\Monaco\rtp1\" #include trailing slash on rtp path

# set the $script_root to the directory where the expected_files.txt lives.  
# this is also where the log files will be written.
$script_root = "\\your_other_server_here\scripts\beam_checksum_script\" #include trailing slash on script_root

# users that should be emailed whenever a change occurs
$email_recipients =  @("user1@toh.on.ca","user2@toh.on.ca","user3@toh.on.ca")

# users to be mailed everytime the script is run (if always_email_admins is not 0)
$admin_emails = @("user1@toh.on.ca","user2@toh.on.ca")
$always_email_admins = 1  #set to 1 to email admins on every run. Set to 0 to disable 

# email will be shown as being sent from this email address
$from_address = "beamscript@toh.on.ca"

# SMTP Email Server Address
$smtp_host= "YOURSMTPSERVER"

