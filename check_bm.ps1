#Randy Taylor(rataylor@toh.on.ca) - 1 Oct 2012
# This script crawls all of the rtp1/*/tele subdirectories of the Monaco beam model
# directories and compares thir MD5 hashes to those found in expected_files.txt
# If a difference in the file checksums are found, the users specified in the 
# $email_recipients array below will be notified.
#
# See settings.ps1 file for configuration options

. .\settings.ps1

$ref_data_file = $script_root+"expected_files.txt"
$log_file = $script_root+"log.txt"


$smtp = New-Object Net.Mail.SmtpClient($smtp_host)	

if (!(Test-Path $rtp_root)){
	Add-Content $log_file "`nunable to access $rtp_root"
	$smtp.Send($from_address,$admin_emails,"beam script unable to access rtp1 directory","$rtp_root")	 
	Exit
}

$start_time = Get-Date;

$bm_search_path = $rtp_root + "*\tele\" 
$tele_files = Get-ChildItem $bm_search_path -Recurse | Where-Object {!$_.PSIsContainer}

$md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
$enc = [system.Text.Encoding]::UTF8

$deleted_files = @()
$changed_files = @()
$new_files = @()

$file_hashes = Import-Csv -Delimiter "|" $ref_data_file
$all_files = @()
$changed_clinics = @{}

foreach ($fhash in $file_hashes){
	
    $all_files += $fhash.file
    $clinic = $fhash.file.Substring($fhash.file.IndexOf("rtp")).Split("\")[1]
    if (!(Test-Path $fhash.file)){
        $changed_files += $fhash.file
        
        if ($changed_clinics.ContainsKey($clinic)){
            $changed_clinics[$clinic] += 1                    
        }else{
            $changed_clinics.set_Item($clinic,1)
        }
        
    }else{
        #hash of file to compare to reference
	    $cmp_file_hash = [System.BitConverter]::ToString($md5.ComputeHash([System.IO.File]::ReadAllBytes($fhash.file)))
        
        if ($fhash.md5 -ne $cmp_file_hash){
			$changed_files += $fhash.file

            if ($changed_clinics.ContainsKey($clinic)){
                $changed_clinics[$clinic] += 1                    
            }else{
                $changed_clinics.set_Item($clinic,1)
            }
		}        
    }	
}

$changed = $changed_files -join "`n"
$changed_count = $changed_files.Length

$new_count = $tele_files.Length - $file_hashes.Length

if ($new_count -gt 0){
    foreach ($tele_file in $tele_files){
        if ($all_files -notcontains $tele_file.Fullname){
            $new_files += $tele_file.Fullname
        }
    }
}
$new = $new_files -join "`n"

$total_count = $tele_files.Length
$end_time = Get-Date;

# log results
$log_contents = "`n$start_time; $end_time; Compared $total_count files; $changed_count Changed files:`n $changed;`n$new_count New files:`n $new;"
Add-Content $log_file $log_contents


# email results if necessary
$email_contents = "`n$start_time; $end_time; Compared $total_count files; $changed_count Changed files; $new_count New files;`nEffected Clinics:"

foreach ($key in $changed_clinics.Keys){
    $clinic_count = $changed_clinics[$key]
    $email_contents += "`n$key : $clinic_count files"
}
$email_contents += "`nDetails available in the logfile at $log_file"

$smtp = New-Object Net.Mail.SmtpClient($smtp_host)	

if (($changed_count -gt 0) -or ($new_count -gt 0) ){	
    $recipients = $email_recipients -join ", "	
    $smtp.Send($from_address,$recipients, "Change to beam files","$email_contents")
}

if ($always_email_admins -gt 0){
    $admin_recipients = $admin_emails -join ", "
	$smtp.Send($from_address,$admin_recipients,"beam script finished running","$email_contents")	 
}
