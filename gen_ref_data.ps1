# This script will crawl all of the tele subdirectories of the Monaco beam model
# directories and output their MD5 hashes to expected_files.txt
#
# Configuration is done via the settings.ps1 file

. .\settings.ps1

#==============================================================================================
$ref_data_file = $script_root+"expected_files.txt"

$delim = "|"

if (Test-Path $ref_data_file){
    $date = Get-Date;
    $old_ref =  "$ref_data_file-" + $date.ToShortDateString().Replace("/",".") 
    mv $ref_data_file $old_ref
}


$bm_search_path = $rtp_root + "*\tele\" 
$tele_files = Get-ChildItem $bm_search_path -Recurse | Where-Object {!$_.PSIsContainer}

$md5 = new-object -TypeName System.Security.Cryptography.MD5CryptoServiceProvider
$enc = [system.Text.Encoding]::UTF8

$stream = [System.IO.StreamWriter] $ref_data_file
$header = "file"+ $delim + "md5" 

$stream.WriteLine($header)

foreach ($fname in $tele_files){	
	
	$cmp_file_hash = [System.BitConverter]::ToString($md5.ComputeHash([System.IO.File]::ReadAllBytes($fname.FullName)))
    $data = $fname.FullName + $delim + $cmp_file_hash
	$stream.WriteLine($data)

}

$stream.Close()

